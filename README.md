# 基于 LeNet5 的手写数字识别

#### 介绍
复现了 基于 LeNet5 的 手写数字识别网络。

#### 软件架构
win10 + python3.8.11 + pytorch1.7.0


#### 安装教程

conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/
conda config --set show_channel_urls yes
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/pytorch/


conda install pytorch==1.7.0 torchvision==0.8.0 torchaudio==0.7.0 cudatoolkit=11.0  pytorch

清华镜像源安装，
Anaconda 管理环境，
pycharm IDE

#### 使用说明

1.  train.py 训练
2.  test.py 测试
3.  Mnist 手写数字数据集下载： http://yann.lecun.com/exdb/mnist/
4.  readMnist.py 里配置 Mnist 路径（ fpath ）
5.  test.py 配置训练的模型
6.  包含了 lenet5.pth 训练好的模型
7.  下面是 lenet5.pth 的测试效果
![输入图片说明](https://images.gitee.com/uploads/images/2021/1005/143411_5e3cae09_9169032.png "捕获.PNG")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
